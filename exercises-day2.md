# Exercise 4.1

### Check out branch `exercise4` and `npm install`.

Look at the navigation in the `App.js`, and the new object-based list of cars in `CarOverview`.

Make a `CarDetails` component that shows details about a given car (brand, model and year is enough for now).

Make a `Details` link in the `CarItem` cards, linking to `/cars/<id>`.

Tip: Use `<Link>` from `react-router-dom`.

Make the app show the `CarDetails` component with the appropriate car, when the user is accessing `/cars/<id>`.

Tip: Import the list of cars from `CarOverview` and find the appropriate car in this list, based on the ID from the URL.

# Exercise 4.2

Make sure, that if a user enters a non-existing car ID, he is sent back to `/cars`.

Tip: Use `<Redirect>` from `react-router-dom`.

# Exercise 5.1

### Check out branch `exercise5` and `npm install`.

Look in the code located in `src/store`. The list of todos is now loaded with a redux thunk, a selector etc.

Now make a selector that gives us all todos crated by a specific user ID. Tip. Use the existing seletor, and `Array.prototype.filter()`.

Create a new component `TodoListByUser` which receives a `userId` prop, and use this new selector in `mapStateToProps`.

Add a few new menu items, linking to `/todos/users/1`, `/todos/users/2` and `/todos/users/3`.

Change the routing logic, so the todos for a specific user can be viewed at `/todos/users/<id>`.

Tip: Use `parseInt()` to convert the string received by the router, to a number.

# Exercise 5.2

Create a new action and reducer logic, for setting a todo to completed in redux.

Tip: Some of the logic for setting the state in Exercise 3 can be reused.

# Exercise 5.3

Create a thunk that does the network API call from exercise 3 to complete the todo, and afterwards fires the action from exercise 5.2.

Tip: You can reuse the HTTP PATCH network call from exercise 3.
