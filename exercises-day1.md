# Exercise 1.1

## Check out the branch `exercise1` before this exercise. (remember `npm install`).

Change `CarOverview` so it shows a list of cars

Tips:

Change `const car` to an array callled `cars`, use `map()` for rendering, and remember a `key` prop in each `<CarItem>` element.

# Exercise 1.2

Make it possible to choose a car in `CarOverview`:

Make an `isSelected` boolean prop on `CarItem`, which sets a `bg=“primary”` prop on the `<Card>` elmement when it's `true`. - So that you have a visual inducation that a car has been chosen.

Make a `handleSelect` method which updates `state` with the chosen car.

Tips:

Make sure that `CarItem` gets a reference to `handleSelect` as its `onSelect` prop.

Make sure to send an `isSelected = true` prop to the car, whose ID matches the car chosen in `this.state`.

# Exercise 2.1

## Check out the branch `exercise2` before this exercise. (remember `npm install`).

With inspiration from the `CarOverview` and `CarItem` components, make a `TodoList` and `TodoItem` component, and make sure that the data model you provide can contain a todo-list.

Copy (parts of) this list into the `TodoList` file, instead of the list of cars:

https://jsonplaceholder.typicode.com/todos

Make sure that your component is compliant with the data format from this list.

Make sure that each has a color depending on whether they are checked or not - explore all options on https://react-bootstrap.github.io/components/cards/#card-styles

Rename the `Select` button to `Done`, Make sure that the botton is only shown on items that are not completed. The `Done` button should do nothing for now.

# Exercise 2.2

Now we want to load the list from the URL, instead of copying the data to our code. Make sure that the `TodoList` component loads the list when it's shown.

Tips:
Store the list in the `state` of the component, instead of a `const`.

Use `componentDidMount` for loading the list - get it using the `fetch` APIet - remember to use `async`/`await`.

# Exercise 3.1

## Check out the branch `exercise3` before this exercise. (remember `npm install`).

A click on `Done` should mark a todo item as complete.

Tip:

Update `state`, and use spreads to copy the resten of `state` to the new state, alogn with the part of state that's updated with `completed: true`. Look at the options for reading the previous state before returning a new object in `this.setState()`.

# Exercise 3.2

Make sure to send a request to a server, when a todo item gets completed.

Create a Requestbin here, in order to mock a REST API.
https://requestbin.com/

Then make an HTTP `PATCH`-request to an URL which looks like the one below (based on the URL you get from RequestBin):

`https://<BIN-ID>.pipedream.net/todos/<TODO-ID>`

With a JSON-body:

```json
{ "completed": true }
```

Read more here about how to make this kind of requests with the Fetch API

https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

Tips:

Rewrite the method which handles clicks on "Done, so that it's an `async` function, and make the execute HTTP-request before `setState`, and make an `await`, so that the local state is only updated when/if the server has acknowledged the request.

Observe how the requests are logged on RequestBin, and how todos are afterwards completed in the user interface.

If something goes wrong, try taking a look in the Chrome Developer tools and check in the "Console" tab, what is being logged. - There might be a hint.
